# homework1

The `milan_full.csv` file contains air pollutants variables for the Milan province in Lombardy region, Italy. They have been preprocessed by considering the [`copernicus` online service](https://copernicus.eu) that is an European project responsible for monitoring e.g. athmosphere and marine environment.


Create the `scripts` folder. Define the `homework1.ipynb` notebook in the `scripts` folder that performs the following steps. Use code cell for the code and markdown cell to describe what you are going to do.  

1. Build a dictionary variable called `aqi` with the following data:

range|value
---|---
<10|Very Good
10-20|Good
20-30|Satisfactory
30-50|Sufficiently
50-80|Poor
80>|Very Poor

2. Build a DataFrame variable called `milan_data` by reading the data in the `./data/milan_full.csv` file. Use the `pd.read_csv()` function. Do not change the header mask before reading the file. 

3. Add the `aqi_value_1` column to the `milan_data` DataFrame by checking the `AQI_1.Av` variable with respect to `aqi['range']`.

4. Add the `aqi_value_2` column to the `milan_data` DataFrame by checking the `AQI_2.Av` variable with respect to `aqi['range']`.

5. Build a bar plot with respect to `milan_data['aqi_value_1']`.

6. Build a bar plot with respect to `milan_data['aqi_value_2']`.

7. Build a bar plot with respect to `milan_data[['aqi_value_1', 'aqi_value_2']`.

8. Explore data by yourself.







